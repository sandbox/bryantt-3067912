<?php

namespace Drupal\annotate_node\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Annotate Node.
 */
class AnnotationConfigurationForm extends ConfigFormBase { 
    
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['annotate_node.settings'];
    
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'annotation_configuration_form';
  }

  /**
   * {@inheritdoc}
   */

  public function buildForm(array $form, FormStateInterface $form_state) {
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
    $options = array();
    foreach ($node_types as $node_type => $val) {
      $options[$node_type] = $val->label();
    }
    $string_options = ['Paragraphs'=>$this->t('Paragraphs'),'Sentences' => $this->t('Sentences'),'Length dependent' => $this->t('Length dependent')];
	$correction_mode_options = ['On'=>$this->t('On'),'Off' => $this->t('Off')];
    
    $config = $this->config('annotate_node.settings');
    $form['annotation_content_types'] = array(
      '#type' => 'radios',
      '#title' => t('Annotation Content Type'),
      '#options' => $options,
      '#description' => $this->t('Please choose the content type where annotation will be allowed.'),
      '#default_value' => $config->get('annotation_content_types'),
    );
    $form['string_separate'] = array(
      '#type' => 'radios',
      '#title' => t('Split String by'),
      '#options' => $string_options,
      '#description' => $this->t('Please select how you would like to seperate content. WARNING - this will change the html of your nodes for the selected content type.  Backup your database.'),
      '#default_value' => $config->get('string_separate'),
      '#attributes' => [
        //define static name and id so we can easier select it
        // 'id' => 'select-length',
        'name' => 'string_separate',
      ],
    );
    $form['custom_length'] = array(
      '#type' => 'textfield',
      '#size' => '60',
      '#placeholder' => 'Number of words at which annotations set by paragraphs and not sentences.',
	  '#default_value' => $config->get('custom_length'),
      '#attributes' => [
        //also add static name and id to the textbox
        //'id' => 'custom_length',
        'name' => 'custom_length',
      ],
      '#states' => [
        //show this textfield only if the radio 'other' is selected above
        'visible' => [
          ':input[name="string_separate"]' => ['value' => 'Length dependent'],
        ],
      ],
    );
	$form['correction_mode'] = array(
		'#type' => 'radios',
		'#description' => $this->t('This will automatically copy the paragraph or sentence into the comment field.  After submission will show changes in red and green.'),
		'#options' => $correction_mode_options,
		'#default_value' => $config->get('correction_mode'),
		'#title' => $this
		->t('Turn on correction mode'),
	);
	$form['annotation_confirmation'] = array(
	  '#type' => 'textfield',
	  '#title' => 'Confirmation displayed',
	  '#default_value' => $config->get('annotation_confirmation'),
	  '#size' => 128,
	  '#maxlength' => 256,
	  '#required' => TRUE,
	);
    

    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('annotation_content_types') != 'annotate_node') {
      //drupal_set_message(t('You have selected a custom content type.  Be sure to copy the node--annotate-node.html.twig in annotate_node/templates and rename it to node--'. $form_state->getValue('annotation_content_types') . '.html.twig  More detailed instructions available in the ReadMe file.'), 'warning');
	  $this->messenger()->addWarning('You have selected a custom content type.  Be sure to copy the node--annotate-node.html.twig in annotate_node/templates and rename it to node--'. $form_state->getValue('annotation_content_types') . '.html.twig  More detailed instructions available in the ReadMe file.');

    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('annotate_node.settings')
      ->set('annotation_content_types', $form_state->getValue('annotation_content_types'))
      ->set('string_separate',$form_state->getValue('string_separate'))
	  ->set('correction_mode',$form_state->getValue('correction_mode'))
	  ->set('annotation_confirmation',$form_state->getValue('annotation_confirmation'))
	  ->set('custom_length',$form_state->getValue('custom_length'))
      ->save();

    parent::submitForm($form, $form_state);
	//drupal_set_message(t('Be sure to clear your caches.' ), 'status');
	$this->messenger()->addStatus('Be sure to clear your caches.');
  }
}
