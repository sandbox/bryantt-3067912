The module allows for comments/corrections/annotations on each sentence or paragraph on static nodes.  It is not intended for nodes that will be edited again after receiving annotations.  It is also very important to note that this module will change the html of the body field for the selected content type in order to divide them into sentences or paragraphs for annotations.  Backup you database first and use the Annotate Node content type first to see how it works.

If you wish to use the annotate node content type to be used to show annotations/corrections, just install as you would any other module and save the configuration at admin/config/annotation-configuration

However, if you would like to use a different content type, just must copy and rename the the file in /annotate_node/templates/node--annotate_node.html.twig.  Rename this file to be node--your_content_type.html.twig.  The content type also needs a body (must be the machine name) as a field.  Set the config at admin/config/annotation-configuration.

If the buttons still do not appear on your content type but work on annotate_node, be sure you don't have another twig already overriding the node for that content type, likely in your theme directory.

Any content types used must have a body field.

The module has some limitations:
	The module numbers each sentence or paragraph and allows annotations on each.  It isn't intended to be used to annotate a node that is still being edited.  Any changes to the document will mess-up the placement of the annotations.
	While it provides the option to break up the text by sentence, it does so by looking for a period and checking spacing.  This isn't always perfect.
	If you change the settings to break text by paragraph to sentence of vice versa, the color coding of changes done	by JavaScript will be messed up on previous comments.

Debugging:
	There's a mutationObserver in the .js file listening for changes on div where id = "content".  If the form doesn't close when commenting, make sure that div exists and covers all content of the node.  Since we're altering html the content, theme, etc could mess up our main div. If that's the problem and it isn't a single page, start by looking in the template provided by the module.  The html is also altered in the hook_preprocess_node.  Can debug by changing the config settings to sentences or paragraphs and seeing when the div breaks.